// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    alias(libs.plugins.androidApplication) apply false
    alias(libs.plugins.jetbrainsKotlinAndroid) apply false
}

ext {
    val androidXVersion by extra("1.3.1")
    val versionCompiler by extra(29)
    val versionTarget by extra(27)
    val minSdkVersion by extra(19)
    val versionCode by extra(126)
    val versionNameString by extra("3.3.3")
    val javaSourceCompatibility by extra(JavaVersion.VERSION_1_8)
    val javaTargetCompatibility by extra(JavaVersion.VERSION_1_8)
    val ndkVersion by extra("21.0.6113669")
    val supportLibVersion by extra("27.1.1")
    val versionBuildTool by extra("27.0.3")
    val kotlinCoreVersion by extra("1.3.2")
    val kotlinCoroutines by extra("1.3.9")
    val materialVersion by extra("1.3.0")
    val constraintlayoutVersion by extra("2.0.4")
    val lifecycle_version by extra("2.2.0")
    val quick_version by extra("2.9.50")
    val dialog_version by extra("3.2.1")
    val bugly_version by extra("3.4.4")
    val bugly_native_version by extra("3.9.0")
}