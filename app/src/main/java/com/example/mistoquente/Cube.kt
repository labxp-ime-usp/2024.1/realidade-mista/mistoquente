package com.example.mistoquente

import android.opengl.GLES20
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer

class Cube {
    private val vertexBuffer: FloatBuffer
    private val colorBuffer: FloatBuffer

    private val vertexShaderCode = """
        uniform mat4 uMVPMatrix;
        attribute vec4 vPosition;
        void main() {
            gl_Position = uMVPMatrix * vPosition;
        }
    """

    private val fragmentShaderCode = """
        precision mediump float;
        uniform vec4 vColor;
        void main() {
            gl_FragColor = vColor;
        }
    """

    // Number of coordinates per vertex in this array
    private val coordsPerVertex = 3

    private val cubeCoords = floatArrayOf(
        -0.5f,  0.5f,  0.5f,   // front top left
        0.5f,  0.5f,  0.5f,   // front top right
        -0.5f, -0.5f,  0.5f,   // front bottom left
        0.5f, -0.5f,  0.5f,   // front bottom right
        -0.5f,  0.5f, -0.5f,   // back top left
        0.5f,  0.5f, -0.5f,   // back top right
        -0.5f, -0.5f, -0.5f,   // back bottom left
        0.5f, -0.5f, -0.5f    // back bottom right
    )

    private val color = floatArrayOf(
        1.0f, 0.0f, 0.0f, 1.0f  // red
    )

    private val drawOrder = shortArrayOf(
        0, 1, 2, 1, 2, 3, // front face
        4, 5, 6, 5, 6, 7, // back face
        0, 1, 4, 1, 4, 5, // top face
        2, 3, 6, 3, 6, 7, // bottom face
        0, 2, 4, 2, 4, 6, // left face
        1, 3, 5, 3, 5, 7  // right face
    )

    private val drawListBuffer = ByteBuffer.allocateDirect(drawOrder.size * 2).run {
        order(ByteOrder.nativeOrder())
        asShortBuffer().apply {
            put(drawOrder)
            position(0)
        }
    }

    private val program: Int

    init {
        // Initialize vertex byte buffer for shape coordinates
        val bb = ByteBuffer.allocateDirect(cubeCoords.size * 4)
        bb.order(ByteOrder.nativeOrder())
        vertexBuffer = bb.asFloatBuffer().apply {
            put(cubeCoords)
            position(0)
        }

        // Initialize color buffer
        val cb = ByteBuffer.allocateDirect(color.size * 4)
        cb.order(ByteOrder.nativeOrder())
        colorBuffer = cb.asFloatBuffer().apply {
            put(color)
            position(0)
        }

        // Prepare shaders
        val vertexShader = MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode)
        val fragmentShader = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode)

        // Create OpenGL program and link shaders
        program = GLES20.glCreateProgram().also {
            GLES20.glAttachShader(it, vertexShader)
            GLES20.glAttachShader(it, fragmentShader)
            GLES20.glLinkProgram(it)
        }
    }

    fun draw(mvpMatrix: FloatArray) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(program)

        // Get handle to vertex shader's vPosition member
        val positionHandle = GLES20.glGetAttribLocation(program, "vPosition")

        // Enable a handle to the cube vertices
        GLES20.glEnableVertexAttribArray(positionHandle)

        // Prepare the cube coordinate data
        GLES20.glVertexAttribPointer(positionHandle, coordsPerVertex, GLES20.GL_FLOAT, false, coordsPerVertex * 4, vertexBuffer)

        // Get handle to fragment shader's vColor member
        val colorHandle = GLES20.glGetUniformLocation(program, "vColor")

        // Set color for drawing the cube
        GLES20.glUniform4fv(colorHandle, 1, color, 0)

        // Get handle to shape's transformation matrix
        val mvpMatrixHandle = GLES20.glGetUniformLocation(program, "uMVPMatrix")

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, mvpMatrix, 0)

        // Draw the cube
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.size, GLES20.GL_UNSIGNED_SHORT, drawListBuffer)

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(positionHandle)
    }
}
