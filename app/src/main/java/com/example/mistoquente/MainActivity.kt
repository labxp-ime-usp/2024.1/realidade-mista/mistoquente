package com.example.mistoquente


import android.content.Context
import android.graphics.PixelFormat
import android.hardware.usb.UsbDevice
import android.view.LayoutInflater
import android.view.View


//import android.app.ActionBar.LayoutParams
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.widget.SeekBar
import androidx.core.view.updateLayoutParams
import android.widget.FrameLayout
import com.example.mistoquente.databinding.ActivityMainBinding

import com.jiangdg.ausbc.MultiCameraClient
import com.jiangdg.ausbc.base.MultiCameraActivity
import com.jiangdg.ausbc.callback.ICameraStateCallBack
import com.jiangdg.ausbc.widget.AspectRatioSurfaceView
import com.jiangdg.ausbc.camera.bean.CameraRequest
import com.jiangdg.ausbc.camera.CameraUVC

class MainActivity : MultiCameraActivity(), ICameraStateCallBack {
    var connectedCameras = 0

    private lateinit var viewBinding: ActivityMainBinding
    val WIDTH_PERCENT = 0.75f

    override fun onCreate(savedInstanceState: Bundle?) {
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        // Adjust spacing
        viewBinding.seekBarSpacing.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                adjustSpacing(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        // Rotate screens
        viewBinding.seekBarRotation.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                rotateImages(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

//        adjustImageSize(viewBinding.frameLayout1, viewBinding.viewFinder1)
//        adjustImageSize(viewBinding.frameLayout2, viewBinding.viewFinder2)
        viewBinding.seekBarSpacing.post { adjustSpacing(0) }
    }

    override fun onCameraAttached(camera: MultiCameraClient.ICamera) {}

    // a camera be detached
    override fun onCameraDetached(camera: MultiCameraClient.ICamera) {}
    override fun generateCamera(ctx: Context, device: UsbDevice): MultiCameraClient.ICamera {
        return CameraUVC(ctx, device)
    }

    // a camera be connected
    override fun onCameraConnected(camera: MultiCameraClient.ICamera) {
        val frameLayout: FrameLayout? = when(connectedCameras) {
            0 -> viewBinding.frameLayout1
            1 -> viewBinding.frameLayout2
            else -> null
        }
        if (frameLayout == null) return
        val view = AspectRatioSurfaceView(this)
        frameLayout.addView(view, FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
        camera.openCamera(view, CameraRequest.Builder().create())
        camera.setCameraStateCallBack(this)
        startGLSurfaceOnFrame(frameLayout)
        connectedCameras += 1
    }

    // a camera be disconnected
    override fun onCameraDisConnected(camera: MultiCameraClient.ICamera) {
        camera.closeCamera()
        connectedCameras -= 1
    }

    override fun getRootView(layoutInflater: LayoutInflater): View? {
        if (viewBinding == null)
            viewBinding = ActivityMainBinding.inflate(layoutInflater)
        return viewBinding?.root
    }

    private fun adjustImageSize(frameLayout: FrameLayout, imageView:  AspectRatioSurfaceView) {
        imageView.post(
            Runnable {
                imageView.updateLayoutParams<FrameLayout.LayoutParams> {
                    height = frameLayout.height
                    width = (WIDTH_PERCENT * frameLayout.width).toInt()
                }
            }
        )

    }

    private fun adjustSpacing(progress: Int) {
        val totalWidth = viewBinding.frameLayout1.width
        val marginSpace = totalWidth * (1f - WIDTH_PERCENT)
        val leftMargin = (marginSpace * (1f - (progress / 100f))).toInt()
        val rightMargin = (marginSpace * (progress / 100f)).toInt()

//        viewBinding.viewFinder1.updateLayoutParams<FrameLayout.LayoutParams> {
//            marginStart = leftMargin
//        }
//        viewBinding.viewFinder2.updateLayoutParams<FrameLayout.LayoutParams> {
//            marginStart = rightMargin
//        }
    }

    private fun rotateImages(rotation: Int) {
        val rot = 90 * rotation.toFloat()
//        viewBinding.viewFinder1.rotation = rot.toFloat()
//        viewBinding.viewFinder2.rotation = rot.toFloat()
    }

    private fun startGLSurfaceOnFrame(frameLayout: FrameLayout) {
        val glSurfaceView = GLSurfaceView(this)
        glSurfaceView.setEGLContextClientVersion(2)
        glSurfaceView.setEGLConfigChooser( 8, 8, 8, 8, 16, 0 )
        glSurfaceView.setRenderer(MyGLRenderer())
        glSurfaceView.setZOrderOnTop(true)
        glSurfaceView.holder.setFormat(PixelFormat.TRANSLUCENT)
        glSurfaceView.renderMode = GLSurfaceView.RENDERMODE_WHEN_DIRTY
        frameLayout.addView(glSurfaceView, FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
    }

    override fun onCameraState(
        self: MultiCameraClient.ICamera,
        code: ICameraStateCallBack.State,
        msg: String?
    ) {
        when (code) {
            ICameraStateCallBack.State.OPENED -> println("Abriu")
            ICameraStateCallBack.State.CLOSED -> println("Fechou")
            ICameraStateCallBack.State.ERROR -> println("Erro")
        }
    }
}